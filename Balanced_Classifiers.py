import numpy as np
import pandas as pd
from sklearn.model_selection import train_test_split
import umap as umap
from sklearn.manifold import TSNE
from sklearn import svm
from sklearn.neighbors import KNeighborsClassifier as KNN
from sklearn.ensemble import RandomForestClassifier as RF
from imblearn.over_sampling import SMOTE, BorderlineSMOTE, RandomOverSampler
from sklearn.metrics import accuracy_score, precision_score, recall_score, f1_score
import matplotlib.pyplot as plt
from sklearn.inspection import DecisionBoundaryDisplay


# function loading data and getting y = dataset.target 
def _prepare_data (path: str, return_type="ndArray"):
    X = np.genfromtxt(path, delimiter=",")
    y = np.array([])

    for i in X:
        y = np.append(y, i[5])
        
    X = np.delete(X, 5, 1)

    X_train, X_test, y_train, y_test = train_test_split(X, y, random_state=0)
    
    if return_type == "ndArray": 
        return np.array(X_train), np.array(X_test), np.array(y_train), np.array(y_test)

    if return_type == "dataframe": 
        X_train = pd.DataFrame(data=X_train)
        X_test = pd.DataFrame(data=X_test)
        y_train = pd.DataFrame(data=y_train)
        y_test = pd.DataFrame(data=y_test)
        return X_train, X_test, y_train, y_test
    
    if return_type == "list":
        #X_train, X_test, y_train, y_test = train_test_split(X, y, random_state=0)
        return X_train.tolist(), X_test.tolist(), y_train.tolist(), y_test.tolist()

# function teaching classifiers and viewing results
def _teach_classifiers (path, man_mode="UMAP", mode="none"):
    X_train, X_test, y_train, y_test = _prepare_data(path=path)
    if mode=="none":
        print('mode=="none"')
    elif mode=="SMOTE":
        print('mode=="SMOTE"')
        X_train, y_train = SMOTE(random_state=42, k_neighbors=1).fit_resample(X_train, y_train)
        X_test, y_test = SMOTE(random_state=42, k_neighbors=1).fit_resample(X_test, y_test)
    elif mode=="borderline_SMOTE":
        print('mode=="borderline-SMOTE"')
        X_train, y_train = BorderlineSMOTE(random_state=42, k_neighbors=1, m_neighbors=1).fit_resample(X_train, y_train)
        X_test, y_test = BorderlineSMOTE(random_state=42, k_neighbors=1, m_neighbors=1).fit_resample(X_test, y_test)
    elif mode=="RandomOverSampler":
        print('mode=="RandomOverSampler"')
        X_train, y_train = RandomOverSampler(random_state=42).fit_resample(X_train, y_train)
        X_test, y_test = RandomOverSampler(random_state=42).fit_resample(X_test, y_test)


    if man_mode == "UMAP":
        X_train = umap.UMAP(n_components=2, n_neighbors=2).fit_transform(X_train)
        X_test = umap.UMAP(n_components=2, n_neighbors=2).fit_transform(X_test)
    elif man_mode == "t_SNE":
        X_train = TSNE(n_components=2).fit_transform(X_train, y_train)
        X_test = TSNE(n_components=2).fit_transform(X_test, y_test)

    

    models = (
        svm.SVC(kernel="rbf", C=1, decision_function_shape="ovr").fit(X_train, y_train),
        KNN(n_neighbors=5, metric='manhattan', weights='distance').fit(X_train, y_train),
        RF(max_depth=5, n_estimators=10, max_features=1).fit(X_train, y_train),
    )

    titles = (
        "SVC with " + mode + " \nbalancer and " + man_mode,
        "kNN with " + mode + " \nbalancer and " + man_mode,
        "RF with " + mode + " \nbalancer and " + man_mode,
    )

    titles_for_metrics = (
        "SVC with " + mode + " balancer and " + man_mode,
        "kNN with " + mode + " balancer and " + man_mode,
        "RF with " + mode + " balancer and " + man_mode,
    )

    _vizualise(models, titles, 1, 3, X_train, X_test, y_train, y_test, titles_for_metrics)

# function counting efficiency of classification
def _functional_metrics (y_test, y_test_pred, title):
        print(f"------- {title} -------")
        print(f"Accuracy score: {accuracy_score(y_test, y_test_pred)}")
        print(f"Precision score: {precision_score(y_test, y_test_pred, labels=y_test, average='micro')}")
        print(f"Recall score: {recall_score(y_test, y_test_pred, labels=y_test, average='macro')}")
        print(f"F1-measure score: {f1_score(y_test, y_test_pred, labels=y_test, average='macro')}")

# function vizualising
def _vizualise (models, titles, size_1, size_2, X_train, X_test, y_train, y_test, titles_for_metrics):
    fig, sub = plt.subplots(size_1, size_2, figsize=(12,3))
    #plt.subplots_adjust(wspace=0.4, hspace=0.4)
    plt.subplots_adjust(wspace=0.4, hspace=0.4, top=0.85)

    X0, X1 = X_train[:, 0], X_train[:, 1]

    for clf, title, ax, metric_title in zip(models, titles, sub.flatten(), titles_for_metrics):
        disp = DecisionBoundaryDisplay.from_estimator(
            clf,
            X_train,
            response_method="predict",
            cmap=plt.cm.coolwarm,
            alpha=0.8,
            ax=ax,
            xlabel="X",
            ylabel="Y",
        )
        ax.scatter(X0, X1, c=y_train, cmap=plt.cm.coolwarm, s=20, edgecolors="k")
        ax.set_xticks(())
        ax.set_yticks(())
        ax.set_title(title)
        # counting metrics 
        y_test_predicted = clf.predict(X_test)
        _functional_metrics(y_test, y_test_predicted, metric_title)

    plt.show()

# function viewing everything
def view (path):
    _teach_classifiers(path=path, man_mode="UMAP", mode="none")
    _teach_classifiers(path=path, man_mode="UMAP", mode="RandomOverSampler")
    _teach_classifiers(path=path, man_mode="UMAP", mode="SMOTE")
    _teach_classifiers(path=path, man_mode="UMAP", mode="borderline_SMOTE")

    _teach_classifiers(path=path, man_mode="t_SNE", mode="none")
    _teach_classifiers(path=path, man_mode="t_SNE", mode="RandomOverSampler")
    _teach_classifiers(path=path, man_mode="t_SNE", mode="SMOTE")
    _teach_classifiers(path=path, man_mode="t_SNE", mode="borderline_SMOTE")



# -------------------- Here comes the main code --------------------
view(path="./hayes+roth/hayes-roth.data")